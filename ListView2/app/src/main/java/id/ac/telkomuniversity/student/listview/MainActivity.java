package id.ac.telkomuniversity.student.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        String negara[] = {"Indonesia", "Malaysia", "Singapura", "Italia", "Inggris", "Belanda", "Argentina", "Chili", "Argentina", "Uganda"};

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = findViewById(R.id.listview);
        ArrayAdapter<String> adapterlist = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1,negara);
        listView.setAdapter(adapterlist);
    }
}
