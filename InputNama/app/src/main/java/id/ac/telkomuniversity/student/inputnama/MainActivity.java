package id.ac.telkomuniversity.student.inputnama;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void tampil(View view) {
        TextView hasil = findViewById(R.id.hasil);
        EditText nama = findViewById(R.id.nama);

        if(nama.getText().length()>0){
            String tampil = nama.getText().toString();
            hasil.setText("Nama saya adalah "+tampil);
        }
    }
}
