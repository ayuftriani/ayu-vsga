package id.ac.telkomuniversity.student.storage;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

public class InternalActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String FILENAME = "namafile.txt";
    private Button buatFile, ubahFile, bacaFile, hapusFile;
    private TextView textBaca;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internal);

        buatFile = findViewById(R.id.buttonBuat);
        ubahFile = findViewById(R.id.buttonUbah);
        bacaFile = findViewById(R.id.buttonBaca);
        hapusFile = findViewById(R.id.buttonHapus);
        textBaca = findViewById(R.id.textBaca);

        buatFile.setOnClickListener(this);
        ubahFile.setOnClickListener(this);
        bacaFile.setOnClickListener(this);
        hapusFile.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        jalankanPerintah(view.getId());
    }

    private void jalankanPerintah(int id) {
        switch (id) {
            case R.id.buttonBuat:
                buatFile();
                break;
            case R.id.buttonUbah:
                ubahFile();
                break;
            case R.id.buttonBaca:
                bacaFile();
                break;
            case R.id.buttonHapus:
                hapusFile();
                break;
        }
    }

    void buatFile() {
        String isiFile = "Coba isi data file text";
        File file = new File(getFilesDir(), FILENAME);

        FileOutputStream fileOutputStream = null;
        try {
            file.createNewFile();
            fileOutputStream = new FileOutputStream(file, true);
            fileOutputStream.write(isiFile.getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void ubahFile() {
        String ubah = "Update isi data file text";
        File file = new File(getFilesDir(), FILENAME);

        FileOutputStream fileOutputStream = null;
        try {
            file.createNewFile();
            fileOutputStream = new FileOutputStream(file, true);
            fileOutputStream.write(ubah.getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void bacaFile() {
        File sdcard = getFilesDir();
        File file = new File(sdcard, FILENAME);

        if (file.exists()) {
            StringBuilder stringBuilder = new StringBuilder();

            try {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
                String line = bufferedReader.readLine();

                while (line != null) {
                    stringBuilder.append(line);
                    line = bufferedReader.readLine();
                }

                bufferedReader.close();
            } catch (IOException e) {
                System.out.println("Error " + e.getMessage());
            }

            textBaca.setText(stringBuilder.toString());
        }

        FileOutputStream fileOutputStream = null;
        try {
            file.createNewFile();
            fileOutputStream = new FileOutputStream(file, false);

            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void hapusFile() {
        File file = new File(getFilesDir(), FILENAME);
        if (file.exists()) {
            file.delete();
        }
    }
}