package id.ac.telkomuniversity.student.sensor;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

public class DeteksiMiring implements SensorEventListener {

    private onMiringListener mListener;

    interface onMiringListener{
        void leftTilted();
        void rightTilted();
    }

    public DeteksiMiring(onMiringListener mListener) {
        this.mListener = mListener;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (Math.abs(sensorEvent.values[0]) <= 2f) return;
        if (sensorEvent.values[0] < 0)
            mListener.leftTilted();
        else
            mListener.rightTilted();
        }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
