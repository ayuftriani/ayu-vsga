package id.ac.telkomuniversity.student.sensor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import java.util.Random;

public class MiringActivity extends AppCompatActivity {

    ImageView mView;

    private DeteksiMiring mDetector;
    private Sensor mAccelerometer;
    private SensorManager mManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miring);

        mView = findViewById(R.id.imageView);
        mManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mDetector = new DeteksiMiring(new DeteksiMiring.onMiringListener() {
            @Override
            public void leftTilted() {
                if (mView.getRight() - 5 < 0)
                    mView.setRight(0);
                else
                    mView.setRight(mView.getLeft()-5);
                Log.d("Miring", "Miring Kiri");
            }

            @Override
            public void rightTilted() {
                if (mView.getLeft() - 5 < 0)
                    mView.setLeft(0);
                else
                    mView.setLeft(mView.getLeft()-5);


                Log.d("Miring", "Miring Kanan");
            }
        });

        }
    }

