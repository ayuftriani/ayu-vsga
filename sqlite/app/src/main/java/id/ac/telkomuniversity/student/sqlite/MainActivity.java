package id.ac.telkomuniversity.student.sqlite;


import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    private EditText nameEditText;
    private Button storeButton, getButton;
    private TextView nameTextView;
    private DatabaseHelper databaseHelper;
    private ArrayList<String> stringArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nameEditText = findViewById(R.id.etname);
        storeButton = findViewById(R.id.btnStore);
        getButton = findViewById(R.id.btnget);
        nameTextView = findViewById(R.id.tvnames);

        databaseHelper = new DatabaseHelper(this);

        storeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                databaseHelper.addStudentDetail(nameEditText.getText().toString());
                nameTextView.setText("");
                nameEditText.setText("");
                Toast.makeText(view.getContext(), "Store Successful!", Toast.LENGTH_SHORT).show();
            }
        });

        getButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stringArrayList = databaseHelper.getAllStudentList();
                nameTextView.setText("");
                for (String names : stringArrayList) {
                    nameTextView.setText(nameTextView.getText().toString() + ", " + names);
                }
            }
        });
    }
}