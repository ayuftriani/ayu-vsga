package id.ac.telkomuniversity.student.kalkulator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText satu, dua;
    Button tambah, kurang, kali, bagi, bersihkan;
    TextView hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        satu = (EditText) findViewById(R.id.satu);
        dua = (EditText) findViewById(R.id.dua);

        tambah = (Button)findViewById(R.id.tambah);
        kurang = (Button)findViewById(R.id.kurang);
        kali = (Button)findViewById(R.id.kali);
        bagi = (Button)findViewById(R.id.bagi);
        bersihkan = (Button) findViewById(R.id.bersihkan);

        hasil = (TextView) findViewById(R.id.hasil);
    }

    public void tambah(View view) {
        if((satu.getText().length()>0) && (dua.getText().length()>0))
        {
            double angka1 = Double.parseDouble(satu.getText().toString());
            double angka2 = Double.parseDouble(dua.getText().toString());
            double result = angka1 + angka2;
            hasil.setText(Double.toString(result));
        }
        else {
            Toast toast = Toast.makeText(MainActivity.this, "Mohon masukkan Angka pertama & Kedua", Toast.LENGTH_LONG);
            toast.show();
        }
}

    public void kurang(View view) {
        if((satu.getText().length()>0) && (dua.getText().length()>0))
        {
            double angka1 = Double.parseDouble(satu.getText().toString());
            double angka2 = Double.parseDouble(dua.getText().toString());
            double result = angka1 - angka2;
            hasil.setText(Double.toString(result));
        }
        else {
            Toast toast = Toast.makeText(MainActivity.this, "Mohon masukkan Angka pertama & Kedua", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public void kali(View view) {
        if((satu.getText().length()>0) && (dua.getText().length()>0))
        {
            double angka1 = Double.parseDouble(satu.getText().toString());
            double angka2 = Double.parseDouble(dua.getText().toString());
            double result = angka1 * angka2;
            hasil.setText(Double.toString(result));
        }
        else {
            Toast toast = Toast.makeText(MainActivity.this, "Mohon masukkan Angka pertama & Kedua", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public void bagi(View view) {
        if((satu.getText().length()>0) && (dua.getText().length()>0))
        {
            double angka1 = Double.parseDouble(satu.getText().toString());
            double angka2 = Double.parseDouble(dua.getText().toString());
            double result = angka1 / angka2;
            hasil.setText(Double.toString(result));
        }
        else {
            Toast toast = Toast.makeText(MainActivity.this, "Mohon masukkan Angka pertama & Kedua", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public void bersih(View view) {
        satu.setText("");
        dua.setText("");
        hasil.setText(String.valueOf(0));
        satu.requestFocus();
    }
}
