package id.ac.telkomuniversity.student.earthquake;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<earthquake> gempas = queryutils.extractGempa();
        ListView gempaListView = findViewById(R.id.listview);
        final EarthquakeAdapter adapter = new EarthquakeAdapter(this, gempas);
        gempaListView.setAdapter(adapter);
    }
}
