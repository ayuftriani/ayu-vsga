package id.ac.telkomuniversity.student.arraylist;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ListView listViewTravel;
    private String[] travelName, travelDescription, travelImage;
    private ArrayList<Travel> travelArrayList;
    private TravelListAdapter travelListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listViewTravel = findViewById(R.id.travel_listview);

        initTravelList();
    }

    private void initTravelList() {
        populateTravel();

        travelListAdapter = new TravelListAdapter(this, travelArrayList);
        listViewTravel.setAdapter(travelListAdapter);
    }

    private void populateTravel() {
        travelName = getResources().getStringArray(R.array.travel_name);
        travelDescription = getResources().getStringArray(R.array.travel_description);
        travelImage = getResources().getStringArray(R.array.travel_image);

        travelArrayList = new ArrayList<>();

        for (int i = 0; i < travelName.length; i++) {
            Travel travel = new Travel();
            travel.setName(travelName[i]);
            travel.setDescription(travelDescription[i]);
            travel.setImage(travelImage[i]);

            travelArrayList.add(travel);
        }
    }
}