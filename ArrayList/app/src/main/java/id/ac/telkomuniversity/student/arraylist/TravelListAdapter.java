package id.ac.telkomuniversity.student.arraylist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class TravelListAdapter extends BaseAdapter {

    private ArrayList<Travel> travelArrayList;
    private Context contextActivity;

    public void setTravelArrayList(ArrayList<Travel> travelArrayList) {
        this.travelArrayList = travelArrayList;
    }

    public TravelListAdapter(Context context, ArrayList<Travel> travelArrayList) {
        contextActivity = context;
        this.travelArrayList = travelArrayList;
    }

    @Override
    public int getCount() {
        return travelArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return travelArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(contextActivity).inflate(R.layout.card_travel, viewGroup, false);
        }

        ViewHolder viewHolder = new ViewHolder(view);
        Travel travel = (Travel) getItem(i);
        viewHolder.bind(travel);

        return view;
    }

    private class ViewHolder {
        private LinearLayout card;
        private TextView name, description;
        private ImageView image;

        ViewHolder (View view) {
            card = view.findViewById(R.id.travel_card);
            name = view.findViewById(R.id.travel_name);
            description = view.findViewById(R.id.travel_description);
            image = view.findViewById(R.id.travel_image);
        }

        void bind(final Travel travel) {
            name.setText(travel.getName());
            description.setText(travel.getDescription());
            Picasso.get().load(travel.getImage()).into(image);
            card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(contextActivity, travel.getName(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
