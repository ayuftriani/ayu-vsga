package id.ac.telkomuniversity.student.soccer;

import com.google.gson.annotations.SerializedName;

public class Player {
    private String idPlayer;

    @SerializedName("strNationality")
    private String nationality;

    @SerializedName("strPlayer")
    private String namePlayer;

    @SerializedName("dateBorn")
    private String birthdate;

    @SerializedName("strBirthLocation")
    private String birthplace;

    @SerializedName("strDescriptionEN")
    private String description;

    @SerializedName("strThumb")
    private String imagePath;

    public Player(String idPlayer, String nationality, String namePlayer, String birthdate, String birthplace, String description, String imagePath) {
        this.idPlayer = idPlayer;
        this.nationality = nationality;
        this.namePlayer = namePlayer;
        this.birthdate = birthdate;
        this.birthplace = birthplace;
        this.description = description;
        this.imagePath = imagePath;
    }

    public String getIdPlayer() {
        return idPlayer;
    }

    public String getNationality() {
        return nationality;
    }

    public String getNamePlayer() {
        return namePlayer;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public String getBirthplace() {
        return birthplace;
    }

    public String getDescription() {
        return description;
    }

    public String getImagePath() {
        return imagePath;
    }
}
