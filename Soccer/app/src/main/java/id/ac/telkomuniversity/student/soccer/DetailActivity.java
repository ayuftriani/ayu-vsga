package id.ac.telkomuniversity.student.soccer;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

public class DetailActivity extends AppCompatActivity {

    private String idPlayer;
    private Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        final ImageView imageView = findViewById(R.id.imageView2);
        final TextView name = findViewById(R.id.name);
        final TextView location = findViewById(R.id.location);
        final TextView birth = findViewById(R.id.birth);
        final TextView nationaity = findViewById(R.id.nationality);
        final TextView desc = findViewById(R.id.desc);

        gson = new Gson();

        idPlayer = getIntent().getStringExtra("idPlayer");
        String url = "https://www.thesportsdb.com/api/v1/json/1/lookupplayer.php?id="+idPlayer;

        //ambil data dengan volley dan GSON
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //ambil nilai dan set ke komponen
                        PlayerResult result = gson.fromJson(response, PlayerResult.class);
                        Player player = result.getPlayer().get(0);
                        name.setText(player.getNamePlayer());
                        nationaity.setText(player.getNationality());
                        location.setText(player.getBirthplace());
                        birth.setText(player.getBirthdate());
                        desc.setText(player.getDescription());

                        Picasso.get().load(player.getImagePath()).into(imageView);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }
        );

        queue.add(stringRequest);
    }
}
