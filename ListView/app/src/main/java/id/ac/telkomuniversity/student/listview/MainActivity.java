package id.ac.telkomuniversity.student.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    String[] pariwisata = {"Saung Udjo Bandung", "Labuan Bajo", "Pulau Belitung", "Gunung Bromo", "Curug Cimahi", "Pantai Pangandaran", "Orchid Forest Lembang", "Situ Patenggang", "Kawah Putih",   "Tangkuban Perahu",
      };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = findViewById(R.id.listview);
        ArrayAdapter <String> adapterlist = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1,pariwisata);
        listView.setAdapter(adapterlist);
    }
}
